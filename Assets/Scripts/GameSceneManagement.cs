﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameSceneManagement : MonoBehaviour
{

    float enemyIntervalMax = 2.0f;
    float enemyIntervalMin = 0.2f;
    float timeToMinimumInterval = 30.0f;

    public Camera mainCamera;
    public Text scoreText;
    public Text gameOverText;
    public ShipContoller ship;
    public GameObject enemeyPrefab;

    Vector3 leftBound;
    Vector3 rightBound;

    int score = 0;
    bool gameOver;
    float gameTimer;
    float enemyTimer;


    void Start()
    {
        Time.timeScale = 1;
        scoreText.enabled = true;
        gameOverText.enabled = false;
        enemyTimer = enemyIntervalMax;
        leftBound = mainCamera.ViewportToWorldPoint(new Vector3(0, 1, -mainCamera.transform.localPosition.z));
        rightBound = mainCamera.ViewportToWorldPoint(new Vector3(1, 0, -mainCamera.transform.localPosition.z));
    }


    void Update()
    {
        GameOverLogic();
        TimeOverLogic();
    }


    void GameOverLogic()
    {
        if (gameOver)
        {
            if (Input.GetKeyDown("r"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            scoreText.enabled = false;
            gameOverText.enabled = true;
            gameOverText.text = "GameOver, Your Score is:" + score + "Press R to Restart";
            return;
        }
    }


    void TimeOverLogic()
    {
        gameTimer += Time.deltaTime;
        enemyTimer -= Time.deltaTime;
        if (enemyTimer <= 0)
        {
            float intervalPercentage = Mathf.Min(gameTimer / timeToMinimumInterval, 1);
            enemyTimer = enemyIntervalMax - (enemyIntervalMax - enemyIntervalMin) * intervalPercentage;
            GameObject enemy = GameObject.Instantiate<GameObject>(enemeyPrefab);
            enemy.transform.SetParent(this.transform.parent);
            enemy.transform.position = new Vector3(Random.Range(leftBound.x, rightBound.x), leftBound.y + 2, 0);
            enemy.GetComponent<EnemyController>().OnKill += OnKillEnemy;
        }
    }


    public void OnKillEnemy()
    {
        score += 100;
        scoreText.text = "score:" + score;
    }


    public void OnGameOver()
    {
        gameOver = true;
        Time.timeScale = 0;
    }
}
