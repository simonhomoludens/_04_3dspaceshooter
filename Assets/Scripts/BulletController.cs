﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    float speed = 20f;

    void Start()
    {

    }


    void Update()
    {
        BulletLogic();
    }


    void BulletLogic()
    {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + speed * Time.deltaTime, this.transform.position.z);
        if (this.transform.position.y > 10)
        {
            GameObject.Destroy(this.gameObject);
        }
    }
}
